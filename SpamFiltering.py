# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import sklearn
import time
#import warnings


start = time. time()
dataset = pd.read_csv("spam.csv", encoding ='ISO-8859-1')#Ucitava fajl iz direktorije rada
dataset.head()

print (dataset)

dataset = dataset.drop(["Unnamed: 2", "Unnamed: 3", "Unnamed: 4"], axis=1)#Odbacujemo nepotrebnih polja
#Ovaj deo je moga biti uradjen i u samom fajlu 
dataset = dataset.rename(columns={"v1":"label", "v2":"text"})#Uzimaju se prve dve kolone i imenuju se Label i text
dataset.head()

dataset["numerical_label"] = dataset.label.map({'ham':0, 'spam':1})#Daju se vrednosti bezopasnim mejlovima i spamu
dataset.head()


print (dataset)
dataset.label.value_counts()


from sklearn.model_selection import train_test_split
#from sklearn.cross_validation import train_test_split

X_train,X_test,y_train,y_test = train_test_split(dataset["text"],dataset["label"], test_size = 0.8, random_state = 10)

print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)
print("AJMO")

from sklearn.feature_extraction.text import CountVectorizer

vect = CountVectorizer(stop_words='english')
vect.fit(X_train)

print(vect.get_feature_names()[0:20])
print(vect.get_feature_names()[-20:])

X_train_df = vect.transform(X_train)
X_test_df = vect.transform(X_test)
type(X_test_df)

prediction = dict()
from sklearn.naive_bayes import MultinomialNB
model = MultinomialNB()
model.fit(X_train_df,y_train)

prediction["naive_bayes"] = model.predict(X_test_df)

from sklearn.metrics import accuracy_score,confusion_matrix,classification_report

accuracy_score(y_test,prediction["naive_bayes"])

print(classification_report(y_test, prediction['naive_bayes'], target_names = ["Ham", "Spam"]))

end = time. time()

print("Vreme izvršenja", end - start)